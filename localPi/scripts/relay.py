#!/usr/bin/env python
import RPi.GPIO as GPIO
import time, sys

relays = []

def setup():
	GPIO.setmode(GPIO.BCM)
	GPIO.setwarnings(False)

	pin1 = 17

	GPIO.setup(pin1,GPIO.OUT)

	relays.append(pin1)

def relaysOn():
	for i in relays:
		time.sleep(0.1)
		GPIO.output(i,GPIO.HIGH)

def relaysOff():
	for i in relays:
		time.sleep(0.1)
		GPIO.output(i, GPIO.LOW)
	
def relayOn(pinNr):
	if pinNr not in relays:
		sys.exit(0)
	time.sleep(0.1)
	GPIO.output(pinNr, GPIO.HIGH)

def relayOff(pinNr):
	if pinNr not in relays:
		sys.exit(0)
	time.sleep(0.1)
	GPIO.output(pinNr, GPIO.LOW)

def clean():
	try:
		GPIO.cleanup()
	except KeyboardInterrupt:
		GPIO.cleanup()	

setup()

