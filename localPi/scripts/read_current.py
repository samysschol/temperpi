#!/usr/bin/env python
import CONFIG_SETTINGS

def get_temp():
	tfile = open("/sys/bus/w1/devices/" + CONFIG_SETTINGS.SENSOR_ID + "/w1_slave")
	text = tfile.read()
	tfile.close()

	tempLine = text.split("\n")[1]
	tempData = tempLine.split(" ")[9]
	temperature = float(tempData[2:])
	temperature = temperature / 1000
	return temperature

print get_temp()
