#!/usr/bin/env python

import subprocess
import CONFIG_SETTINGS

def getStatus():
	batcmd="ssh -p 22345 pi@" + CONFIG_SETTINGS.EXTERNAL_IP + " 'python /home/pi/TemperPi/scripts/get_status.py 1'" 	#1 = <rpi_id>
	outp = subprocess.check_output(batcmd, shell=True)

	temp = outp.split("\n")[0]

	return temp
