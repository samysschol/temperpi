#!/bin/bash

PID=`(ps aux | grep /home/pi/TemperPi/scripts/lamp_branden_als_temp | grep python | awk '{ print $2 }')`
if [ $PID > 0 ]; then
	sudo kill $PID && sudo python /home/pi/TemperPi/scripts/gpio_cleanup
fi
