#!/usr/bin/env python

import time, calendar, subprocess, datetime 
from datetime import date
import CONFIG_SETTINGS

datumText = calendar.day_name[date.today().weekday()].lower()
# == Tuesday/Wednesday...

s = time.strftime('%d/%m/%Y')

tijd = time.mktime(datetime.datetime.strptime(s, "%d/%m/%Y").timetuple())

#dus starttijd = tijd + second

batcmd="ssh -p 22345 pi@" + CONFIG_SETTINGS.EXTERNAL_IP + " 'python /home/pi/TemperPi/scripts/get_time_active.py 1'"
outp = subprocess.check_output(batcmd, shell=True)

res = outp.split('\n')

begin = tijd + float(res[0])
eind = tijd + float(res[1])

print "begin: " + str(begin)
print "end: " + str(eind)
print "active: " + res[2]
