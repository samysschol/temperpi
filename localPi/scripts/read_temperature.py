#!/usr/bin/env python

import subprocess
import CONFIG_SETTINGS

def get_desired_temperature():
	batcmd="ssh -p 22345 pi@" + CONFIG_SETTINGS.EXTERNAL_IP  + " 'python /home/pi/TemperPi/scripts/get_desired.py 1'"
	outp = subprocess.check_output(batcmd, shell=True)

	temp = outp.split("\n")[0]

	return temp

def get_temperature( ):
	tfile = open("/sys/bus/w1/devices/" + CONFIG_SETTINGS.SENSOR_ID + "/w1_slave")
	text = tfile.read()
	tfile.close()

	tempLine = text.split("\n")[1]
	tempData = tempLine.split(" ")[9]
	temperature = float(tempData[2:])
	temperature = temperature / 1000
	return temperature

def get_max_temperature():
        tfile = open("/home/pi/TemperPi/scripts/temperatures.txt")
        text = tfile.read()
        tfile.close()

        max = text.split("\n")[1]
        max = max.split(":")[1]
        return max
	

def get_min_temperature():
        tfile = open("/home/pi/TemperPi/scripts/temperatures.txt")
        text = tfile.read()
        tfile.close()

        min = text.split("\n")[2]
        min = min.split(":")[1]
        return min
