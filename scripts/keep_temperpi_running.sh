#!/bin/sh
#enkel 'sudo mongod' killen
#Op eigen systeem:	wc -l geeft altijd minstens 1 aangezien de 'grep' zelf al 1 lijn geeft
#Via ssh: 		wc -l geeft altijd minstens 0 aangezien het niet op het eigen systeem is

LOCAL_IP="192.168.1.10"
echo $LOCAL_IP

#zien of mongod aan het runnen is
MONGO="$(ps aux | grep 'mongod' | grep 'sudo' | grep -Ev 'grep' | wc -l)"
MONGO=$((MONGO))
echo "$MONGO"

API="$(ps aux | grep 'api' | grep 'sudo\|python' | grep -Ev 'grep' | wc -l)"
API=$((API))
echo "$API"

LAMP="$(ssh -p 22345 pi@$LOCAL_IP ps aux | grep 'lamp_branden_als_temp' | wc -l)"
LAMP=$((LAMP))
echo "$LAMP"

if [ $MONGO -eq 1 ]
then
echo "Mongo staat op"
else
echo "Mongo staat NIET op"
fi

if [ $API -ge 1 ]
then
echo "API staat op"
else
echo "API staat NIET op"
fi

if [ $LAMP -eq 1 ]
then
echo "CV staat op"
else
echo "CV staat NIET op"
fi

if [ $LAMP -eq 1 ] && [ $MONGO -eq 1 ] && [ $API -ge 1 ]
then
	echo "Alle 3 staan al op"
else
	echo "NIET alle 3 staan op"
	echo "Killing all processes..."
	#ze staan niet allemaal op dus voor zekerheid alles eerst killen
	if [ $MONGO -eq 1 ]
	then
		MONGO_PID=`(ps aux | grep 'mongod' | grep 'sudo' | grep -Ev 'grep' | awk '{ print $2 }')`
		if [ $MONGO_PID > 0 ]
		then
			sudo kill $MONGO_PID
		fi
	fi

	if [ $API -ge 1 ]
	then
		API_PID1=`(ps aux | grep 'api' | grep 'sudo' | grep -Ev 'grep' | awk '{ print $2 }')`
                if [ $API_PID1 > 0 ]
		then
                        sudo kill $API_PID1
                fi
             	API_PID2=`(ps aux | grep 'api' | grep ' python' | grep -Ev 'grep' | awk '{ print $2 }')`
                if [ $API_PID2 > 0 ]
		then
                        sudo kill $API_PID2
                fi
                API_PID3=`(ps aux | grep 'api' | grep '/usr/bin/python' | grep -Ev 'grep' | awk '{ print $2 }')`
                if [ $API_PID3 > 0 ]
                then
                        sudo kill $API_PID3
                fi
	fi

	if [ $LAMP -eq 1 ]
	then
		ssh -p 22345 pi@$LOCAL_IP sh /home/pi/TemperPi/scripts/lamp_uitzetten.sh
	fi


	echo "Restarting all processes..."
	#alles terug opzetten
	sudo service mongodb stop
	sudo mongod --dbpath=/home/pi/TemperPi/data/db/ &
	sudo TemperPi/scripts/api &
	ssh -p 22345 pi@$LOCAL_IP python /home/pi/TemperPi/scripts/lamp_branden_als_temp &
fi
