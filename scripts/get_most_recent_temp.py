#!/usr/bin/env python

from pymongo import MongoClient
import json, sys
from bson import json_util, ObjectId

rpi_id = int(sys.argv[1])

client = MongoClient('localhost:27017')
db = client.eve

owner = ""

for doc in db.temperpi.find({"rpi_id":rpi_id}):
	json_dump = json.dumps(doc, default=json_util.default)
	
	data = json.loads(json_dump)
	owner = data['_id']['$oid']


for doc in db.logs.find({"owner": ObjectId(owner)}).sort("_created",-1):
	json_dump = json.dumps(doc, default=json_util.default)

	data = json.loads(json_dump)
	print data['temperature']
	break;
