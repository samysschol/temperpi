#!/usr/bin/env python
import sys, json, calendar
from pymongo import MongoClient
from bson import json_util
from datetime import date

client = MongoClient()

db = client.eve

id = sys.argv[1]

result = db.timer.find({'rpi_id':id})

datumText = calendar.day_name[date.today().weekday()].lower()

for i in result:
	json_dump = json.dumps(i, default=json_util.default)
	
	data = json.loads(json_dump)

	temp = json.dumps(data[str(datumText)])

	outp = json.loads(temp)	

	print outp['begin_time']
	print outp['end_time']
	print outp['active']


#oproepen: python get_time_active.py <rpi_id>

#outp : begin /n end /n active
