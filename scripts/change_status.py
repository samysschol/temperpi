#!/usr/bin/env python

from pymongo import MongoClient
import json
from bson import json_util
import sys

client = MongoClient()
db = client.eve

# on/off word meegegeven in het script en zo de status op True/False gezet
if len(sys.argv) > 2: 
	if sys.argv[1] == "on" or sys.argv[1] == "On" or sys.argv[1] == "1":
		print "Status: On"
		result1 = db.temperpi.update_one({'rpi_id':int(sys.argv[2])}, {"$set": { "status" : True } })

	elif sys.argv[1] == "off" or sys.argv[1] == "Off" or sys.argv[1] == "0":
		print "Status: Off"
		result1 = db.temperpi.update_one({'rpi_id':int(sys.argv[2])}, {"$set": { "status" : False } })
