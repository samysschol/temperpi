#!/usr/bin/env python
#post_temp_log.py <rpi_id> <ip_addr_rpi>
import os, subprocess, requests, urllib2, json, sys
from pymongo import MongoClient
from bson import json_util, ObjectId

client = MongoClient()
db = client.eve
temperpi = db.temperpi.find()

rpi_id = sys.argv[1]
ip_addr = sys.argv[2]

batcmd="ssh -p 22345 pi@" + ip_addr  + " 'python /home/pi/TemperPi/scripts/read_current.py'"
outp = subprocess.check_output(batcmd, shell=True)

temperpi = db.temperpi.find({'rpi_id':int(rpi_id)})

temp = outp.split("\n")[0]

owners = []

for doc in temperpi:
	json_data = json.dumps(doc, default=json_util.default)
	data = json.loads(json_data)
	id = data['_id']
	idArray = json.dumps(id)
	oid = json.loads(idArray)
	owners.append(json.dumps(oid["$oid"]))

i = 0
for owner in owners: 
	url = 'http://localhost:5000/logs'
	data = '{"owner":' + owners[i] + ',"temperature":' + temp + '}'
	headers = '{"Content-Type":"application/json"}'
	req = urllib2.Request(url, data, {'Content-Type':'application/json'})
	f = urllib2.urlopen(req)
	response = f.read()
	f.close()
	i = i + 1
	print response
