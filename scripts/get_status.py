#!/usr/bin/env python

from pymongo import MongoClient
import json, sys
from bson import json_util

client = MongoClient('localhost:27017')
db = client.eve

result = db.temperpi.find({'rpi_id':int(sys.argv[1])})

id = -1

for i in result:
	json_dump = json.dumps(i, default=json_util.default)
	
	data = json.loads(json_dump)

	print data['status']

