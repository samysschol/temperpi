XML = False
X_DOMAINS = '*'
RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PUT', 'PATCH', 'DELETE']
temperpi = {
		'schema': {
			'rpi_id': {
				'type': 'integer',
				'required': True,
				'unique': True,
			},
			'name': {
				'type': 'string',
			},
			'status': {
				'type': 'boolean',
				'default': False,
			},
			'desired_temperature': {
				'type': 'float',
				'default': '21',
			},
		},
		'additional_lookup': {
                        'url': 'regex("[\w]+")',
			'field': 'rpi_id',
    	}
}

logs = {
 		'schema': {
			'temperature': {
				 'type':'float',
			},
			'owner': {
				'type': 'objectid',
				'required' : True,
				'data_relation': {
					'resource' : 'temperpi',
					'embeddable': True,
				},
			},
		}
}

accounts = {
		'schema': {
			'username': {
				'type': 'string',
				'required': True,
				'unique': True,
			},
			'password': {
				'type': 'string',
				'required': True,
			},
			'token': {
				'type': 'string',
			},
			'roles': {
				'type': 'list',
				'allowed': ['user', 'superuser', 'admin'],
				'required': True,
			},
		},
		# the standard account entry point is defined as
		# '/accounts/<ObjectId>'. We define  an additional read-only entry
		# point accessible at '/accounts/<username>'.
		'additional_lookup': {
			'url': 'regex("[\w]+")',
			'field': 'username',
		},
		# We also disable endpoint caching as we don't want client apps to
		# cache account data.
		'cache_control': '',
		'cache_expires': 0,

		# Only allow superusers and admins.
		'allowed_roles': ['superuser', 'admin'],
}

timer = {
	'schema': {
		'rpi_id': {
                          'type': 'string',
                          'required' : True,
                },
		'monday':{
                        'type': 'dict',
                        'schema': {
				#time is in seconds (60seconds -> 1min)
                                'begin_time': { 'type': 'integer', 'default':0 },
                                'end_time': { 'type': 'integer', 'default':0 },
                                'active': { 'type': 'boolean', 'default': False }
                        }
                },
                'tuesday':{
                        'type': 'dict',
                        'schema': {
                                #time is in seconds (60seconds -> 1min)  
                                'begin_time': { 'type': 'integer', 'default':0 },
                                'end_time': { 'type': 'integer', 'default':0 },
                                'active': { 'type': 'boolean', 'default': False }
                        }
                },
                'wednesday':{
                        'type': 'dict',
                        'schema': {
                                #time is in seconds (60seconds -> 1min)  
                                'begin_time': { 'type': 'integer', 'default':0 },
                                'end_time': { 'type': 'integer', 'default':0 },
                                'active': { 'type': 'boolean', 'default': False }
                        }
                },
                'thursday':{
                        'type': 'dict',
                        'schema': {
                                #time is in seconds (60seconds -> 1min)  
                                'begin_time': { 'type': 'integer', 'default':0 },
                                'end_time': { 'type': 'integer', 'default':0 },
                                'active': { 'type': 'boolean', 'default': False }
                        }
                },
                'friday':{
                        'type': 'dict',
                        'schema': {
                                #time is in seconds (60seconds -> 1min)  
                                'begin_time': { 'type': 'integer', 'default':0 },
                                'end_time': { 'type': 'integer', 'default':0 },
                                'active': { 'type': 'boolean', 'default': False }
                        }
                },

                'saturday':{
                        'type': 'dict',
                        'schema': {
                                #time is in seconds (60seconds -> 1min)  
                                'begin_time': { 'type': 'integer', 'default':0 },
                                'end_time': { 'type': 'integer', 'default':0 },
                                'active': { 'type': 'boolean', 'default': False }
                        }
                },
                'sunday':{
                        'type': 'dict',
                        'schema': {
                                #time is in seconds (60seconds -> 1min)  
                                'begin_time': { 'type': 'integer', 'default':0 },
                                'end_time': { 'type': 'integer', 'default':0 },
                                'active': { 'type': 'boolean', 'default': False }
                        }
                },
	}
}

DOMAIN = {
	'temperpi' : temperpi,
	'logs' : logs,
	'accounts': accounts,
	'timer': timer,
}

