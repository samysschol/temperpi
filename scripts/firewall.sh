#!/bin/bash

#indien de API ook op de lokale machines bereikbaar moet zijn, laat deze dan toe via IPtables
sudo iptables -A INPUT -p tcp --dport 5000 -s localhost -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 5000 -j DROP
sudo iptables -A INPUT -p tcp --dport 27017 -s localhost -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 27017 -j DROP
