<?php
if(isset($_GET['rpi_id'])){
	$pi_id = str_replace("'", "", $_GET['rpi_id']);
	setDesired($pi_id);
}

function setDesired($rpi_id) {
	$temperature = $_POST['desired_temperature']; 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_URL, 'http://localhost:5000/temperpi/' . $rpi_id  . '/desired/' . $temperature);
        $result = curl_exec($ch);
        curl_close($ch);
	header('Location: /pages/index.php');
}
?>
