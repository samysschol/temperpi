<?php
	function reset_all($rpi_id){
		//rpi_id moet meegegeven zijn zodat je weet welke timer
		//day moet meegegeven zijn zodat je weet welke dag je moet aanpassen voor de tijd
		$m = new MongoClient();
	        $db = $m->eve;
	        $collection = $db->timer;

		$days = array("monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday");

		foreach($days as $day){
			$newdata = array('$set' => array("$day" => array("begin_time" => "0", "end_time" => "0", "active" => false)));
			$collection->update(array("rpi_id" => "$rpi_id"), $newdata);
		}
	}
	$rpi_id= $_POST["rpi_id"];
	reset_all($rpi_id);
	header('Location: /pages/edit_timer.php?rpi_id=' . $rpi_id);
?>
