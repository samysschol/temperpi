<?php
session_start();

if (isset($_SESSION['token']) && !empty($_SESSION['token']) && isset($_SESSION['username']) && !empty($_SESSION['username'])) {
        if(checkToken($_SESSION['username'], $_SESSION['token'])) {
                //AUTHORIZED
        } else {
                sendToLogin();
        }
} else {
        sendToLogin();
}
?>
