<?php
function pc_validate($user,$pass) {
    $m = new MongoClient();
    $db = $m->eve;
    $collection = $db->accounts;

    $cursor = $collection->find();

    $gevonden = false;
    $token = "";
    $username = "";
    foreach ($cursor as $document) {
	if (($document["username"] == $user) && ($document["password"] == sha1($pass))) {
		$gevonden = true;
		$token = $document["token"];
		$username = $document["username"];
	}
    }

    if ($gevonden) {
	$_SESSION['username'] = $username;
	$_SESSION['token'] = $token;
	return true;
    } else {
        return false;
    }
}

function sendToLogin() {
	header('WWW-Authenticate: Basic realm="Temperpi dashboard"');
	header('HTTP/1.0 401 Unauthorized');
	header('Location: /pages/login.php');
	exit;
}

function checkToken($user, $token) {
    $m = new MongoClient();
    $db = $m->eve;
    $collection = $db->accounts;

    $cursor = $collection->find();

    $gevonden = false;
    foreach ($cursor as $document) {
        if (($document["username"] == $user) && ($document["token"] == $token)) {
                $gevonden = true;
        }
    }

    return $gevonden;
}
?>
