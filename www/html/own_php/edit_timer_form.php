<?php
	$rpi_id= $_POST["rpi_id"];
	$day = $_POST["day"];

	$activated = $_POST['activated'] === 'true'? true: false;

	$begin_hour = 0;
	$begin_minute = 0;
	$end_hour = 0;
	$end_minute = 0;

	$begin_time = 0;
	$end_time =0;

	if(isset($_POST["begin_hour"]) && isset($_POST["begin_minute"]) && isset($_POST["end_hour"]) && isset($_POST["end_minute"])){

		if(intval($_POST["begin_hour"]) >= 0 && intval($_POST["begin_hour"]) <= 23 && intval($_POST["begin_minute"]) >= 0 && intval($_POST["begin_minute"]) <= 59){
			$begin_hour= intval($_POST["begin_hour"]);
			$begin_minute= intval($_POST["begin_minute"]);
		}
		if(intval($_POST["end_hour"]) >= 0 && intval($_POST["end_hour"]) <= 23 && intval($_POST["end_minute"]) >= 0 && intval($_POST["end_minute"]) <= 59){
		        $end_hour= intval($_POST["end_hour"]);
		        $end_minute= intval($_POST["end_minute"]);
		}

		$begin_time= ($begin_hour*3600) + ($begin_minute*60);
                $end_time = ($end_hour*3600) + ($end_minute*60);
		//indien begintime groter is dan endtime dan is er een fout en mag het niet opgeslagen worden --> alles op 0 zetten en activated op false
		if( $begin_time > $end_time){
        		$begin_time = 0;
        		$end_time =0;
			$activated = false;
		}
	}

	function edit_day($rpi_id, $day, $begin_time, $end_time, $activated){
		//rpi_id moet meegegeven zijn zodat je weet welke timer
		//day moet meegegeven zijn zodat je weet welke dag je moet aanpassen voor de tijd
		$m = new MongoClient();
	        $db = $m->eve;
	        $collection = $db->timer;

		$newdata = array('$set' => array("$day" => array("begin_time" => "$begin_time", "end_time" => "$end_time", "active" => $activated)));
		$collection->update(array("rpi_id" => "$rpi_id"), $newdata);
	}

//dag en rpi_id moeten nog meegegeven worden door html
	edit_day($rpi_id,$day, $begin_time, $end_time, $activated);
	$send_id = intval($rpi_id);
	header('Location: /pages/edit_timer.php?rpi_id=' . $rpi_id);
?>
