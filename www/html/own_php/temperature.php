
<?php
if(isset($_GET['rpi_id'])){
        $pi_id = $_GET['rpi_id'];
	getTemp($pi_id);
}

function getTemp($rpi_id) {
		$m = new MongoClient();
                $db = $m->eve;
                $collectionLogs = $db->logs;
                $collectionTemperPi = $db->temperpi;

		$rpi_id = intval($rpi_id);
		$temperpi_query = array('rpi_id' =>$rpi_id);
                $cursor = $collectionTemperPi->find($temperpi_query);
		$obj_id = -1;
		foreach($cursor as $doc){
			$obj_id = $doc['_id'];
		}

		$logs_query = array('owner' => $obj_id);
		$logs_sort = array('_created' => -1);
		$cursor2 = $collectionLogs->find($logs_query)->sort($logs_sort)->limit(1);

		//cursor2 is zo gesorteerd dat de laatste log vanboven zit --> daarom na eerste loop al stoppen eigenlijk om deze te returnen

		//we moeten enkel de eerste log hebben
		foreach($cursor2 as $doc){
			if(empty($doc['temperature'])){
				echo "N/A";
			}else{
				echo round($doc['temperature'],1);
			}
			break;
                }

}
?>
