<?php
	$username= $_POST["username"];
	$password= $_POST["password"];

	header("Authorization: Basic " . $username . ":" . $password);
	require_once('../own_php/authenticate.php');
	session_start();
	$_SESSION["username"] = $username;
	$_SESSION["password"] = $password;
	if(pc_validate($_SESSION["username"], $_SESSION["password"])) {
		header('Location: /pages/index.php');
	} else {
		header('Location: /pages/login.php');
	}

?>
