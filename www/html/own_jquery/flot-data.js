//Flot Bar Chart
$(document).ready(function() {
	var div = document.getElementById("dom-target-temperatures");
        var myDataTemp = div.textContent.trim();
	myDataTemp = JSON.parse("[" + myDataTemp + "]");

        var divDate = document.getElementById("dom-target-dates");
        var myDataDate = divDate.textContent.trim();
        myDataDate = JSON.parse("[" + myDataDate + "]");

	var date_temp= [];
	for (i = 0; i < myDataDate.length; i++) { 
		var date_tmp = new Date(myDataDate[i]);
		var tmp = [date_tmp, myDataTemp[i]];
   		date_temp.push(tmp);
	}

    var barOptions = {
        series: {
            bars: {
                show: true,
                barWidth: 3600000
            }
        },
        xaxis: {
            mode: "time",
            timeformat: "%d %b %Hu",
            minTickSize: [1, "hour"]
        },
        grid: {
            hoverable: true
        },
        legend: {
            show: false
        },
        tooltip: true,
        tooltipOpts: {
            content: "Tijd: %x Temperatuur: %y"
        }
    };
    var barData = {
        label: "bar",
	data: date_temp
    };
    $.plot($("#flot-bar-chart"), [barData], barOptions);

});
