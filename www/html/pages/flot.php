<?php require_once('../own_php/authenticate.php');
include('../own_php/check_auth.php');

if (isset($_GET['destroy'])){
	session_destroy();
	sendToLogin();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Temper Pi - Charts </title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
<!--    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet"> -->

    <!-- Timeline CSS -->
<!--    <link href="../css/timeline.css" rel="stylesheet"> -->

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
<!--    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet"> -->

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

	<!-- Navigation -->
      <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php">Temper Pi</a>
          </div>
          <!-- /.navbar-header -->

          <ul class="nav navbar-top-links navbar-right">
              <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                      <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-user">
		      <li><a href="?destroy"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                      </li>
                  </ul>
                  <!-- /.dropdown-user -->
              </li>
              <!-- /.dropdown -->
          </ul>
          <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
		      <li>
                          <a href="index.php"><i class="fa fa-desktop fa-fw"></i> Dashboard</a>
                      </li>
			<li>
                            <a href="flot.php"><i class="fa fa-bar-chart-o fa-fw"></i> Charts</a>
                        </li>
                        <li>
                            <a href="munin.php"><i class="fa fa-bar-chart-o fa-fw"></i> Munin</a>
                        </li>
                        <li>
                            <a href="tables.php"><i class="fa fa-table fa-fw"></i> Logs</a>
                        </li>
                      <li>
                          <a href="?destroy"><i class="fa fa-sign-in fa-fw"></i> Logout</a>
                          <!-- /.nav-second-level -->
                      </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Graph</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
			<a href="flot.php"><i class="fa fa-refresh"></i></a>
                            Bar Chart Temperatures
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="flot-chart">
                                <div class="flot-chart-content" id="flot-bar-chart"></div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
	<div id="dom-target-temperatures" style="display: none;">
    	<?php	$ch = curl_init();
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        $link = "http://localhost:5000/logs/1/";
	        curl_setopt($ch, CURLOPT_URL, $link );
	        $result = curl_exec($ch);
	        curl_close($ch);
	        $obj = json_decode($result,true);

		$counter = 1;
	        $rij = "";
		$total= count($obj['_items']);
	        foreach($obj['_items'] as $item){
			if ( $counter == $total-1 ) {
                                        $rij = $rij . htmlspecialchars(json_encode($item['temperature']));
                        }
			elseif ($counter % 60 == 0) {
				if ( $counter < $total ){
					$rij = $rij . htmlspecialchars(json_encode($item['temperature'])) . ",";
				}
	        	}
			$counter = $counter + 1;
		}
		echo htmlspecialchars($rij);?>
	</div>
	<div id="dom-target-dates" style="display: none;">
	<?php   $ch = curl_init();
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        $link = "http://localhost:5000/logs/1/";
	        curl_setopt($ch, CURLOPT_URL, $link );
	        $result = curl_exec($ch);
	        curl_close($ch);
	        $obj = json_decode($result,true);

		date_default_timezone_set('UTC');
                //De 3 dagen ophalen (vandaag, gisteren en eregisteren)
                $dag1 = date('Ymd',strtotime("-2 day"));
                $dag3 = date('Ymd',strtotime("-1 day"));
                $dag2 = date('Ymd',strtotime("now"));

		$counter = 1;
	        $rij = "";
		$total= count($obj['_items']);
	        foreach($obj['_items'] as $item){
			$datetime = filter_var(htmlspecialchars(json_encode($item['_created'])), FILTER_SANITIZE_NUMBER_INT);

			$effdate = date('Ymd', preg_replace('/[^\d]/','', $datetime)/1000);
			if($effdate == $dag3 || $effdate == $dag2 || $effdate == $dag1){
				if ( $counter == $total-1 ) {
                                	        $rij = $rij . htmlspecialchars($datetime);
                        	}
				if ($counter % 60 == 0) {
                        	        if ( $counter < $total ){
						$rij = $rij . htmlspecialchars($datetime) . ",";
					}
				}
			}
			$counter = $counter + 1;
	        }
		$rij = trim($rij);
		$rij = rtrim($rij, ",");

		echo htmlspecialchars($rij);?>
        </div>
	<script>
    		var div = document.getElementById("dom-target-temperatures");
    		var myData = div.textContent;
	</script>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Flot Charts JavaScript -->
    <script src="../bower_components/flot/excanvas.min.js"></script>
    <script src="../bower_components/flot/jquery.flot.js"></script>
    <script src="../bower_components/flot/jquery.flot.pie.js"></script>
    <script src="../bower_components/flot/jquery.flot.resize.js"></script>
    <script src="../bower_components/flot/jquery.flot.time.js"></script>
    <script src="../bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
     <script src="../own_jquery/flot-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
</body>

</html>
