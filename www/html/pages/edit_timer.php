<?php require_once('../own_php/authenticate.php');
include('../own_php/check_auth.php');

if (isset($_GET['destroy'])){
	session_destroy();
	sendToLogin();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Temper Pi - Edit timer </title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
<!--    <link href="../css/timeline.css" rel="stylesheet"> -->

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

	<!-- Navigation -->
      <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php">Temper Pi</a>
          </div>
          <!-- /.navbar-header -->

          <ul class="nav navbar-top-links navbar-right">
              <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                      <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-user">
		      <li><a href="?destroy"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                      </li>
                  </ul>
                  <!-- /.dropdown-user -->
              </li>
              <!-- /.dropdown -->
          </ul>
          <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
		      <li>
                          <a href="index.php"><i class="fa fa-desktop fa-fw"></i> Dashboard</a>
                      </li>
			<li>
                            <a href="flot.php"><i class="fa fa-bar-chart-o fa-fw"></i> Charts</a>
                        </li>
                        <li>
                            <a href="munin.php"><i class="fa fa-bar-chart-o fa-fw"></i> Munin</a>
                        </li>
                        <li>
                            <a href="tables.php"><i class="fa fa-table fa-fw"></i> Logs</a>
                        </li>
                      <li>
                          <a href="?destroy"><i class="fa fa-sign-in fa-fw"></i> Logout</a>
                          <!-- /.nav-second-level -->
                      </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
            <!-- /.navbar-static-side -->
        </nav>
	<?php $rpi_id = (int) $_GET["rpi_id"]; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit timer - TemperPi N°<?php echo $rpi_id; ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
		<div class="pull-right">
		<form role="form" action='../own_php/reset_timer_form.php' method="POST" style="none">
			<input value="Reset all" type="submit" class="btn btn-xs btn-default btn-block">
			<input type="hidden" name="rpi_id" value="<?php echo $rpi_id; ?>">
		</form>
		</div>
            <div class="row">
                <!-- /.col-lg-12 -->
                <div class="col-lg-12">
		<?php
			$ch = curl_init();
                	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
               		$link = 'http://localhost:5000/timer?where={"rpi_id":"' . $rpi_id . '"}';
               		curl_setopt($ch, CURLOPT_URL, $link );
               		$result = curl_exec($ch);

                	curl_close($ch);
               		$obj = json_decode($result, true);

			$items = $obj['_items'];
			//controleren of er wel een object bestaat met deze rpi_id
			$rpi_id_exists = $obj['_meta']['total'];
			if($rpi_id_exists == 0){
				header('Location: /pages/index.php');
			}

			$days = array("monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday");
			 for($i = 0; $i<count($days); $i++){
				$day_obj=$items[0][$days[$i]];
				$begin_time = $day_obj['begin_time'];
				$end_time = $day_obj['end_time'];
				//activated moet obj zijn omdat het een boolean bevat
				$activated = $day_obj['active'];
				// begin_time en end_time omzettne naar uren en minuten
				$begin_hour = floor($begin_time/3600);
				$begin_minute = ($begin_time % 3600) / 60;
				$end_hour = floor($end_time / 3600);
				$end_minute = ($end_time % 3600) / 60;
		?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
			<i class="fa fa-calendar"></i>
                            <b><?php echo ucfirst($days[$i]); ?></b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
				<div class="row">
					<form role="form" action='../own_php/edit_timer_form.php' method="POST" style="none">
						<!-- Begin uur & min -->
                                            	<div class="col-lg-12">
							<label>Begin Timer</label>
						</div>
						<div class="col-lg-12">
							<div class="col-lg-6">
								<label>Hour</label>
		                           			 <select name="begin_hour" class="form-control">
									<?php
										for($x = 0; $x <= 23; $x++) {
									?>
                                           		     		<option <?php if ($x == $begin_hour){ echo "selected"; }?>><?php echo $x; }?></option>
                	                            		</select>
							</div>

							<div class="col-lg-6">
								<label>Minute</label>
                                                		<select name="begin_minute" class="form-control">
									<?php
                                                        	                for($x = 0; $x <= 59; $x++) {
                                                        	        ?>
                                                        	        <option <?php if ($x == $begin_minute){ echo "selected"; }?>><?php echo $x; }?></option>
	                                                	</select>
							</div>
						 </div>
						<!-- /Begin uur & min -->

						<!-- Eind uur & min -->
                                                <div class="col-lg-12">
                                                        <label>End Timer</label>
                                                </div>
                                                <div class="col-lg-12">
                                                        <div class="col-lg-6">
								<label>Hour</label>
                                                         	<select name="end_hour" class="form-control">
                                                                	<?php
                                                                	        for($x = 0; $x <= 23; $x++) {
                                                                	?>
                                                                	<option <?php if ($x == $end_hour){ echo "selected"; }?>><?php echo $x; }?></option>
                                                        	</select>
                                                        </div>

                                                	<div class="col-lg-6">
								<label>Minute</label>
        	                                                <select name="end_minute" class="form-control">
                	                                                <?php
                        	                                                for($x = 0; $x <= 59; $x++) {
                                	                                ?>
                                        	                        <option <?php if ($x == $end_minute){ echo "selected"; }?>><?php echo $x; }?></option>
                                                	        </select>
                                                        </div>
                                                </div>
                                                <!-- /Eind uur & min -->

					<!-- activated -->
					<div class="col-lg-12">
					  <label>Activated</label>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="activated" value="true" <?php if($activated){ echo "checked";}?>>Yes
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="activated" value="false" <?php if($activated == false) { echo "checked"; }?>>No
                                                </label>
                                            </div>
					</div>
					<!-- /activated -->

						<div class="col-lg-12">
							<span class="pull-right">
								<input type="hidden" name="rpi_id" value="<?php echo $rpi_id; ?>">
                                                                <input type="hidden" name="day" value="<?php echo $days[$i];?>">
								<input value="Save" type="submit" class="btn btn-md btn-default btn-block">
							</span>
						</div>
					</form>
				</div> <!-- /row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
		<?php } ?>

                </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
</body>

</html>
