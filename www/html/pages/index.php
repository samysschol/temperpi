<?php require_once('../own_php/authenticate.php');
include('../own_php/check_auth.php');
require_once('../own_php/status.php');
require_once('../own_php/temperature.php');

if (isset($_GET['destroy'])){
	session_destroy();
	sendToLogin();
}
?>

</html>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
<!-- cache geheugehn verwijderen -->
	<meta http-equiv="pragma" content="no-cache" />

    <title>Temper Pi - Home</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <!-- <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet"> -->

    <!-- Timeline CSS -->
    <!-- <link href="../dist/css/timeline.css" rel="stylesheet"> -->

    <!-- Morris Charts CSS -->
    <!-- <link href="../bower_components/morrisjs/morris.css" rel="stylesheet"> -->

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">
<body>

  <div id="wrapper">

      <!-- Navigation -->
      <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php">Temper Pi</a>
          </div>
          <!-- /.navbar-header -->

          <ul class="nav navbar-top-links navbar-right">
              <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                      <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-user">
		      <li><a href="?destroy"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                      </li>
                  </ul>
                  <!-- /.dropdown-user -->
              </li>
              <!-- /.dropdown -->
          </ul>
          <!-- /.navbar-top-links -->

          <div class="navbar-default sidebar" role="navigation">
              <div class="sidebar-nav navbar-collapse">
                  <ul class="nav" id="side-menu">
                      <li>
                          <a href="index.php"><i class="fa fa-desktop fa-fw"></i> Dashboard</a>
                      </li>
                        <li>
                            <a href="flot.php"><i class="fa fa-bar-chart-o fa-fw"></i> Charts</a>
                        </li>
                        <li>
                            <a href="munin.php"><i class="fa fa-bar-chart-o fa-fw"></i> Munin</a>
                        </li>
                        <li>
                            <a href="tables.php"><i class="fa fa-table fa-fw"></i> Logs</a>
                        </li>
                      <li>
                          <a href="?destroy"><i class="fa fa-sign-in fa-fw"></i> Logout</a>
                          <!-- /.nav-second-level -->
                      </li>

                  </ul>
              </div>
              <!-- /.sidebar-collapse -->
          </div>
          <!-- /.navbar-static-side -->
      </nav>

      <div id="page-wrapper">
          <div class="row">
              <div class="col-lg-12">
                  <h1 class="page-header">Dashboard</h1>
              </div>
              <!-- /.col-lg-12 -->
          </div>
          <!-- /.row -->


<!-- Panel per Pi -->
<!-- <div class="row"> -->
<!-- <div class="col-lg-12"> -->
<?php
	$m = new MongoClient();
        $db = $m->eve;
        $collection = $db->temperpi;

        $cursor = $collection->find();
        $desired = "";
        foreach ($cursor as $id => $value) {
?>
	<div class="panel panel-default">
	<div class="panel-heading">
	<?php echo $value["name"]; ?>
	</div>

	<div class="panel-body">
<!-- vanaf hier begint de oorspronkelijke row -->

          <div class="row">
              <div class="col-lg-4 col-md-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          <div class="row">
				<div class="col-xs-3" id="status_icon<?php echo $value['rpi_id'];?>">
				<?php if($value['status']){?>
                                  <i class="fa fa-sun-o fa-5x"></i>
				<?php }else{ ?>
				  <i class="fa fa-moon-o fa-5x"></i>
				<?php }?>
                                </div>
                              <div class="col-xs-9 text-right">
                                  <div id='pi_status<?php echo $value['rpi_id']; ?>' class="huge"><?php getStatus($value["rpi_id"]);?></div>
                                  <div>Status</div>
                              </div>
                          </div>
                      </div>

                  </div>
              </div>
              <div class="col-lg-4 col-md-6">
                  <div class="panel panel-primary">
                      <div class="panel-heading">
                          <div class="row">
                              <div class="col-xs-3">
                                  <i class="fa fa-dashboard fa-5x"></i>
                              </div>
                              <div class="col-xs-9 text-right">
                                  <div><span id='temp<?php echo $value["rpi_id"]; ?>' class="huge"><?php echo getTemp($value["rpi_id"]);?></span> °C</div>
                                  <div>Temperature</div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

	<form role="form" action='../own_php/post_temperature.php?rpi_id=<?php echo $value["rpi_id"];?>' method="POST" style="none">
              <div class="col-lg-4 col-md-6">
                  <div class="panel panel-yellow">
<!-- change desired -->
                      <div class="panel-heading">
                          <div class="row">
                              <div class="col-xs-3">
                                  <i class="fa fa-gear fa-5x"></i>
                              </div>

                              <div class="col-xs-9 text-right">
					<span class="pull-right">
							<select name="desired_temperature" class="form-control">
                                                        <?php
									if ($value["desired_temperature"]) {
										$desired = $value["desired_temperature"];
									}
                                                                for($x = 16; $x <= 30; $x = $x + 0.5) {
                                                                        if( $x == $desired ){
                                                                                echo "<option selected>" . $x . "</option>";
                                                                        }else{
                                                                                echo "<option>" . $x . "</option>";
                                                                        }
                                                                }
                                                        ?>
                                                 	</select>
							<input value="Change" type="submit" class="btn btn-xs btn-default btn-block" style="margin: 4 0 0 0;">
                                                 	<div>Desired Temperature</div>
					</span>
                              </div><!-- col-xs-9 -->
                          </div><!-- row -->
                      </div><!-- panel-heading -->
                  </div>
              </div>
<!-- /change desired temp -->
	</form>

<!-- timer tonen -->
		<div class="col-lg-12">
                  <div class="panel panel-default">
                      <div class="panel-heading">
			<a href="edit_timer.php?rpi_id=<?php echo $value['rpi_id'];?>"><i class="fa fa-gear"></i></a>
			Timer
			</div>
                          <div class="panel-body">


			<div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Day of the week</th>
                                            <th>Begin time</th>
					    <th>End time</th>
					    <th>Actived</th>
                                        </tr>
                                    </thead>
                                    <tbody>
<?php
			$temp_id = $value["rpi_id"];

			$ch = curl_init();
                	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
               		$link = 'http://localhost:5000/timer?where={"rpi_id":"' . $temp_id . '"}';
               		curl_setopt($ch, CURLOPT_URL, $link );
               		$result = curl_exec($ch);

                	curl_close($ch);
               		$obj = json_decode($result, true);

			$items = $obj['_items'];
			//controleren of er wel een object bestaat met deze rpi_id
			$rpi_id_exists = $obj['_meta']['total'];

			$days = array("monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday");
			 for($i = 0; $i<count($days); $i++){
				$day_obj=$items[0][$days[$i]];
				$begin_time = $day_obj['begin_time'];
				$end_time = $day_obj['end_time'];
				//activated moet obj zijn omdat het een boolean bevat
				$activated = $day_obj['active'];
				// begin_time en end_time omzettne naar uren en minuten
				$begin_time_formatted = gmdate("H:i", $begin_time);
				$end_time_formatted = gmdate("H:i", $end_time);

				$color = "'danger'";
				$actief = "No";
				if($activated){ $color = "'success'"; $actief = "Yes"; }
				//color toevoegen als activated is groen anders rood
				$rij = "<tr class=" . $color . "><td>" . htmlspecialchars(ucfirst($days[$i])) . "</td><td>" . htmlspecialchars($begin_time_formatted) . "</td><td>" . htmlspecialchars($end_time_formatted) . "</td><td>" . htmlspecialchars($actief)  .  "</td></tr>";
				echo $rij;
			}
?>
                                    </tbody>
                                </table>
                            </div>


                      </div>
                  </div>
              </div>

<!-- /timer tonen -->


	</div>
	 <!-- einde row -->

</div>
<!-- einde div panel body -->
</div>
<?php } ?>

<!-- einde div panel per pi -->
<!-- </div> -->
<!-- einde col klasse lg12 van panel per Pi -->
<!-- </div> -->
<!-- einde row -->
<!-- /Panel per pi-->

      </div>
      <!-- /#page-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- deze js moet erin blijven voor LOGOUT -->
  <!-- jQuery -->
  <script src="../bower_components/jquery/dist/jquery.min.js"></script>

  <!-- jQuery -->
  <!-- Bootstrap Core JavaScript -->
  <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

  <!-- Metis Menu Plugin JavaScript -->
  <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

  <!-- Morris Charts JavaScript -->
<!--  <script src="../bower_components/raphael/raphael-min.js"></script> -->
<!--  <script src="../bower_components/morrisjs/morris.min.js"></script> -->
<!--  <script src="../js/morris-data.js"></script> -->

  <!-- Custom Theme JavaScript -->
  <script src="../dist/js/sb-admin-2.js"></script>


  <script>
	//Aantal temperpi's ophalen
	//Ophalen van status, elke 5 sec
	var auto_refresh = setInterval(
	<?php include('../own_php/get_total_amount_rpis.php');
		$total = getAmount();
	?>
		(function () {
			<?php
				for($i=1; $i<$total + 1; $i++){
				 	?>
					//$("#temp<?php echo $i;?>").load('../own_php/temperature.php?rpi_id=<?php echo $i; ?>');
                                        $("#pi_status<?php echo $i;?>").load('../own_php/status.php?rpi_id=<?php echo $i; ?>');
					$("#status_icon<?php echo $i;?>").load('../own_php/get_status_icon.php?rpi_id=<?php echo $i; ?>');
				<?php }?>
		}
	), 5000);
  </script>
<script>
        //Aantal temperpi's ophalen
	//Ophalen temperatuur, elke 30 sec
        var auto_refresh = setInterval(
                (function () {
                        <?php
                                for($i=1; $i<$total + 1; $i++){
                                        ?>
                                        $("#temp<?php echo $i;?>").load('../own_php/temperature.php?rpi_id=<?php echo $i; ?>');
                                        //$("#pi_status<?php echo $i;?>").load('../own_php/status.php?rpi_id=<?php echo $i; ?>');
                                        //$("#status_icon<?php echo $i;?>").load('../own_php/get_status_icon.php?rpi_id=<?php echo $i; ?>');
                                <?php }?>
                }
        ), 30000);
  </script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->


</body>

</html>
